require "crud_uploader/engine"

module CrudUploader

  # The parent controller the CrudUploader controller inherits from.
  # Defaults to ApplicationController.  This should be set early
  # in the initialization process, and should be a string
  mattr_accessor :parent_controller
  @@parent_controller = "ApplicationController"

  def self.setup
    yield self
  end

end
